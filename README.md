# ghostpdl 9.51

See:
https://ghostscript.com/doc/9.51/Readme.htm

Any bugs should be reported to:
https://bugs.ghostscript.com/

(Please do not use the github.com issue tracker).
